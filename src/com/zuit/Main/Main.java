package com.zuit.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        int i = 0;
        PhoneBook phonebook = new PhoneBook();
        while(i == 0) {
            System.out.println("Menu\n1.addContact\n2.Display all contacts\n3.Exit");
            System.out.println("Select a number to proceed:");
            int n = Integer.parseInt(sc.nextLine());
            if(n == 1) {
                Contact newContact = new Contact();
                System.out.println("Add a contact:");
                System.out.println("Enter name:");
                newContact.setName(sc.nextLine());
                System.out.println("Enter 2 numbers:");
                String[] num = new String[2];

                for (int counter = 0; counter < 2; counter++){
                    num[counter] = sc.nextLine();
                }

                newContact.setNumbers(num);
                sc.nextLine();

                System.out.println("Enter your permanent and present address:");
                String[] add = new String[2];

                for (int counter = 0; counter < 2; counter++){
                    add[counter] = sc.nextLine();
                }

                newContact.setAddresses(add);
                sc.nextLine();

                PhoneBook.addContact(newContact);
                sc.nextLine();
            }
            if(n == 2) {

                if (PhoneBook.viewAllContacts().size() == 0) {
                    System.out.println("PhoneBook is Empty.");
                } else {
                    System.out.println("The contact in the List are:");
                    ArrayList<Contact> obj = PhoneBook.viewAllContacts();
                    for (Contact temp:obj) {
                        System.out.println(temp.getName());

                        for (int numb = 0; numb < temp.getNumbers().length; numb++) {

                            System.out.println("number " + (numb + 1) + ":" + temp.getNumbers()[numb]);
                        }

                        for (int j = 0; j < temp.getAddresses().length; j++) {
                            System.out.println("Address " + (j + 1) + ":" + temp.getAddresses()[j]);
                        }

                        sc.nextLine();
                }

                }
            }

            if(n==3) {
                System.exit(0);
            }
        }
    }
}
