package com.zuit.Main;

public class Contact extends PhoneBook {

    private String name;
    private String[] numbers;
    private String[] addresses;



    public Contact() {}

    public Contact(String name, String[] numbers, String[] addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;

    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getNumbers() {

        return numbers;
    }
    public void setNumbers(String[] numbers) {

        this.numbers = numbers;
    }

    public String[] getAddresses() {

        return addresses;
    }

    public void setAddresses (String[] addresses) {

        this.addresses = addresses;
    }

}
