package com.zuit.Main;


import java.util.ArrayList;

public class PhoneBook {

    private static ArrayList<Contact> Phonebook = new ArrayList<Contact>();

    public ArrayList<Contact> getPhonebook() {
        return Phonebook;
    }

    public void setPhonebook(ArrayList<Contact> phonebook) {
        this.Phonebook = phonebook;
    }

    public static void addContact(Contact contactObj) {
        Phonebook.add(contactObj);
    }

    public static ArrayList<Contact> viewAllContacts() {

        return Phonebook;
    }
}
